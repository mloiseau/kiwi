# fruity with keyboardS

A [LimeSurvey](https://www.limesurvey.org/) [theme](https://manual.limesurvey.org/Themes) extending [Fruity](https://github.com/LimeSurvey/LimeSurvey/tree/master/themes/survey/fruity), for the specific use of testing children.

It allows :
* to create text questions, each associated with a specific keyboard layout ;
* to block the progression of children on a specific page to be unlocked by a questionnaire administrator.

## Resources
This project depends on limesurvey and uses [jkeyboard](https://github.com/javidan/jkeyboard).

Backgrounds adapted from [birdscards](https://www.birdscards.com/free-svg-cut-files/background-svgs/).
