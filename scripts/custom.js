/******************
	User custom JS
	---------------

   Put JS-functions for your template here.
   If possible use a closure, or add them to the general Template Object "Template"
*/

var cheat = 5;

$(document).on('ready pjax:scriptcomplete',function(){
	/**
	 * Code included inside this will only run once the page Document Object Model (DOM) is ready for JavaScript code to execute
	 * @see https://learn.jquery.com/using-jquery-core/document-ready/
	 */
	handleExtrakeyboard();
	handleEnd();
});

function handleExtrakeyboard(){
	inputs = $(".ecrire input");
	layouts = $(".ecrire .clavier");
	res = inputs.length > 0 && layouts.length == inputs.length;
	if (res){
		for(i=0;i<inputs.length;i++){
			input = $(inputs[i])
			layout = $(layouts[i])
			layout = createLayout(input, layout.text(), i);
			createKeyboard(input, layout, i) ;
		}
	}
	else if(inputs.length > 0){
		alert(inputs.length+" questions écrire / "+layouts.length+" claviers définis…");
	}
	return res;
}

function createLayout(i, l, n){
	res = {
			layout: "l"+n,
			input: i,
			customLayouts: {
				selectable: ["l"+n],
			}
		};
	layout = [] ;
	line = [];
	console.log(l);
	l = l.replace("espace","␠").replace("retour","␈").replace("entrée","␍");
	console.log(l);
	for(j=0;j<l.length;j++){
		if(l[j]==" "){
			layout.push(line);
			line=[];
		}
		else{
		    switch(l[j]){
		        case "␠":
		            line.push("space");
		            break;
		        case "␈":
		            line.push("backspace");
		            break;
		        case "␍":
		            line.push('return');
		            break;
		        default:
		            line.push(l[j]);
		    }
		}
	}
	if(line.length != 0){
		layout.push(line);
	}
	//layout[0].push('backspace');
	res['customLayouts']["l"+n] = layout ;
	return res;
}

function createKeyboard(input, layout, n){
	elt = $( "<div id='kb"+n+"'></div>" );
	elt.appendTo(input.parent());
	input.addClass("jkeyboard-answer");
	$('#kb'+n).jkeyboard(layout);
	input.attr('readonly','readonly');
}

function cheatCode(){
    cheat--;
    elt = $("<p>"+cheat+"</p>");
    $('.fin').append(elt);
    setTimeout(function(){
        elt.remove();
        if(cheat === 0){
            code = prompt('Entrer le code de déverouillage :');
            if(code == $("[code]")[0].getAttribute('code')){
                $('#ls-button-submit').prop('disabled', false);
            }
            else{
                cheat = 5;
            }
        }
    }, 100);
}

function handleEnd(){
    if(testEnd()){
        submit = $('#ls-button-submit');
        submit.prop('disabled', true);
        $(".fin").click(cheatCode);
    }
    
}

function testEnd(){
    elt = $(".fin");
    return elt.length>0;
}